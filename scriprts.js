/**
 * Created by Andrew on 23.07.2016.
 */
$(document).ready(function(){
    getCurrenciesViaPhp();
});


function getCurrenciesViaPhp(){
    var aCources = [];
    $.ajax({
        dataType: "json",
        url: '/functions.php',
        method: 'post',
        data: {
            do: 'getCurrencies'
        },
        success: displayData
    });
}

function displayData(currencies){
    currencies.forEach(function(item, i, arr){
        var row = $('#template').tmpl(item);
        if(item.SaleDelta > 0){
            $(row).find('.sale span').addClass('glyphicon glyphicon-chevron-up');
        }
        else if(item.SaleDelta < 0) {
            $(row).find('.sale span').addClass('glyphicon glyphicon-chevron-down');
        }
        if(item.BuyDelta > 0){
            $(row).find('.buy span').addClass('glyphicon glyphicon-chevron-up');
        }
        else if(item.BuyDelta < 0) {
            $(row).find('.buy span').addClass('glyphicon glyphicon-chevron-down');
        }
        row.appendTo('#currencies')
    });
}

$(document).ajaxStart(function(){
    $('#loader').show();
    $('#main-content').hide();
}).ajaxStop(function(){
    $('#loader').hide();
    $('#main-content').show();
});